﻿using System;
using System.Globalization;
using System.Linq;
using System.Xml;

namespace KFDExporter
{
   public class XmlExporter : IExporter
   {
      private readonly XmlWriter _writer;
      public XmlExporter( string filename )
      {
         var settings = new XmlWriterSettings
         {
            Indent = true, 
            NewLineOnAttributes = true
         };
         _writer = XmlWriter.Create( filename, settings );
      }

      public void Prepare()
      {
         _writer.WriteStartElement( "topics" );
         var now = String.Format( "{0:G}", DateTime.Now );
         _writer.WriteAttributeString( "exportedOn", now );
      }

      public void Finish()
      {
         _writer.WriteEndElement();
         _writer.Flush();
      }

      public void Export(Discussion discussion)
      {
         ExportDiscussion( discussion );
      }

      private void ExportDiscussion(Discussion discussion)
      {
         _writer.WriteStartElement( "topic" ); 
         _writer.WriteElementString( "threadId", discussion.ThreadId );
         _writer.WriteElementString( "title", discussion.Title );
         _writer.WriteElementString( "author", discussion.Author );
         _writer.WriteElementString( "creationDate", discussion.CreationDate );
         _writer.WriteElementString( "category", discussion.Category );
         _writer.WriteElementString( "numReplies", discussion.NumReplies );

         ExportPosts( discussion );

         _writer.WriteEndElement();
      }

      private void ExportPosts( Discussion discussion )
      {
         _writer.WriteStartElement( "posts" );
         foreach ( var post in discussion.Posts.Select( kvPair => kvPair.Value ) )
         {
            ExportOnePost( post );
         }
         _writer.WriteEndElement();
      }

      private void ExportOnePost( Post post )
      {
         _writer.WriteStartElement( "post" ); 
         _writer.WriteElementString( "postId", post.PostId );
         _writer.WriteElementString( "postNumber", post.Number );

         _writer.WriteStartElement( "subject" );
         _writer.WriteCData( post.Subject );
         _writer.WriteEndElement();

         _writer.WriteElementString( "author", post.Author );
         _writer.WriteElementString( "creationDate", post.CreationDate );
         _writer.WriteElementString( "editDate", post.EditDate );

         _writer.WriteStartElement( "body" );
         _writer.WriteCData( post.Text );
         _writer.WriteEndElement();

         _writer.WriteEndElement();
      }
   }
}
