﻿using System;
using System.IO;

namespace KFDExporter
{
   class Program
   {
      public static Options CommandlineOptions { get; private set; }

      private static void Main( string[] args )
      {
         CommandlineOptions  = new Options();
         if ( !CommandLine.Parser.Default.ParseArguments( args, CommandlineOptions ) )
         {
            return;
         }

         CommandlineOptions.OutputType = CommandlineOptions.OutputType.ToLower();
         if ( CommandlineOptions.OutputType != "xml" && CommandlineOptions.OutputType != "text" )
         {
            Console.WriteLine( CommandlineOptions.GetUsage() );
            return;
         }

         File.Delete( CommandlineOptions.OutputFilename );
         File.Delete( CommandlineOptions.ErrorFilename );

         IExporter exporter;
         if ( CommandlineOptions.OutputType == "xml" )
         {
            exporter = new XmlExporter( CommandlineOptions.OutputFilename );
         }
         else
         {
            exporter = new TextExporter( CommandlineOptions.OutputFilename );
         }
         exporter.Prepare();

         var processor = new FileProcessor( exporter );
         var directories = Utility.FindDirectories( CommandlineOptions.Source );
         if ( directories.Length != 0 )
         {
            processor.ProcessDirectories( directories );
         }
         else
         {
            var filenames = Utility.FindFiles( CommandlineOptions.Source );
            if ( filenames.Length != 0 )
            {
               processor.ProcessFiles( filenames );
            }
            else
            {
               Console.WriteLine( "Could not find any files to process in {0}", args[0] );
            }
         }
         exporter.Finish();
      }
   }
}
