﻿using System;

namespace KFDExporter
{
   [Serializable]
   public class PostParsingException : Exception
   {
      public PostParsingException()
      {
      }
      public PostParsingException( string message )
         : base( message )
      {
      }
      public PostParsingException( string message, Exception inner )
         : base( message, inner )
      {
      }
      protected PostParsingException(
       System.Runtime.Serialization.SerializationInfo info,
       System.Runtime.Serialization.StreamingContext context )
         : base( info, context )
      {
      }
   }
}
