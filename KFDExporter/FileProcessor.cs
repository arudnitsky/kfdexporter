﻿using System;
using System.IO;
using HtmlAgilityPack;

namespace KFDExporter
{
   class FileProcessor
   {
      private Discussion _discussion;
      private readonly IExporter _exporter;

      public static string CurrentFilename { get; private set; }

      public FileProcessor( IExporter exporter )
      {
         _exporter = exporter;
      }

      public void ProcessDirectories( string[] directories )
      {
         foreach ( var directory in directories )
         {
            var filenames = Utility.FindFiles( Path.Combine( directory, "*.html" ) );
            if ( filenames.Length == 0 )
            {
               continue;
            }
            Console.WriteLine( directory );
            ProcessFiles( filenames );
         }
      }

      public void ProcessFiles( string[] filenames )
      {
         _discussion = null;
         foreach ( var filename in filenames )
         {
            try
            {
               ProcessOneFile( filename );
            }
            catch ( MalformedHtmlFileException e )
            {
               var message = String.Format( "Malformed HTML in {0}: {1}\r\n", filename, e.Message );
               File.AppendAllText( Program.CommandlineOptions.ErrorFilename, message );
            }
            catch ( PostParsingException e )
            {
               var message = String.Format( "Error parsing posts in {0}: {1}\r\n", filename, e.Message );
               File.AppendAllText( Program.CommandlineOptions.ErrorFilename, message );
            }
         }

         if ( _discussion != null )
         {
            _exporter.Export( _discussion );
         }
      }

      private void ProcessOneFile( string filename )
      {
         CurrentFilename = filename;

         var doc = new HtmlDocument();
         doc.Load( filename );

         var discussion = new Discussion();
         discussion.Parse( doc );
         if ( _discussion == null )
         {
            _discussion = discussion;
         }

         if ( _discussion.ThreadId != discussion.ThreadId )
         {
            throw new ParsingException("Files must have the same ThreadId" );
         }

         var postParser = new PostParser();
         var posts = postParser.Parse( doc );
         _discussion.MergePosts( posts );
      }
   }
}
