﻿using CommandLine;
using CommandLine.Text;

namespace KFDExporter
{
   public class Options
   {
      [Option( 'i', "input", Required = true, HelpText = "Input file location. Can be a directory of subdirectories of threads OR a single html file OR a wildcard filepattern" )]
      public string Source { get; set; }

      [Option( 'o', "output", DefaultValue = "Output.xml", HelpText = "Export destination")]
      public string OutputFilename { get; set; }

      [Option( 'e', "errors", DefaultValue = "Errors.txt", HelpText = "Error log")]
      public string ErrorFilename { get; set; }

      [Option( 't', "type", DefaultValue = "xml", HelpText = "Output type. Can be either xml or text")]
      public string OutputType { get; set; }

      [HelpOption]
      public string GetUsage()
      {
         var help = new HelpText
         {
            Heading = new HeadingInfo( "KFDExporter", "- A utility to export KFD threads from Wetpaint as XML or text" ),
            AddDashesToOption = true
         };
         help.AddOptions( this );

         //   Console.WriteLine( "Example: KFDExporter \\kennethfolkdharma.wetpaint.com\\thread" );
         //   Console.WriteLine( "         KFDExporter PracticeLog.html" );
         //   Console.WriteLine( "         KFDExporter *.html" );

         return help;
      }
   }
}
