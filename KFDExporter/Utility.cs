﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace KFDExporter
{
   public static class Utility
   {
      public static string RemoveNbsp( string input )
      {
         return input.Replace( "&nbsp;", "" );
      }

      public static string CleanHtml( string input )
      {
         string output;

         output = input.Replace( "&nbsp;", " " );
         output = output.Replace( "&amp;", "&" );
         output = output.Replace( "&#39;", "'" );
         output = output.Replace( "&#039;", "'" );
         output = output.Replace( "&rsquo;", "'" );
         output = output.Replace( "&lsquo;", "'" );
         output = output.Replace( "&rdquo;", "'" );
         output = output.Replace( "&ldquo;", "'" );
         output = output.Replace( "<blockquote>", "" ); 
         output = output.Replace( "</blockquote>", "\r\n\r\n" );
         output = output.Replace( "<br>", "\r\n" );
         output = Regex.Replace( output, @"<[^>]*>", String.Empty );

         return output;
      }

      public static string GetDate( string dateIn )
      {
         var date = Utility.FixDate( dateIn ); //Aug 11 2011, 4:33 PM EDT -> Aug 11 2011 4:33 PM
         var postDate = DateTime.ParseExact( date, "MMM d yyyy h:mm tt", CultureInfo.InvariantCulture );
         return postDate.ToString();
      }

      private static string FixDate( string dateIn )
      {
         char[] delimiters = { ' ', ',', ':' };
         string[] dateParts = dateIn.Split( delimiters, StringSplitOptions.RemoveEmptyEntries );
         return dateParts[0] + " " + dateParts[1] + " " + dateParts[2] + " " + dateParts[3] + ":" + dateParts[4] + " " + dateParts[5];
      }

      public static string[] FindFiles( string path )
      {
         string directory = Path.GetDirectoryName( path );
         string filePattern = Path.GetFileName( path );

         if ( String.IsNullOrEmpty( directory ) )
         {
            directory = Directory.GetCurrentDirectory();
         }

         if ( !Directory.Exists( directory ) )
         {
            return new string[0];
         }

         var files = Directory.GetFiles( directory, filePattern );
         return files;
      }

      public static string[] FindDirectories( string path )
      {
         string[] directories;

         path = path.Trim( new[] { '"' } );
         path = path.TrimEnd( new[] { '"', '/', '\\' } );
         try
         {
            directories = Directory.GetDirectories( path );
         }
         catch ( DirectoryNotFoundException )
         {
            directories = new string[0];
         }
         catch ( ArgumentException )
         {
            directories = new string[0];
         }
         return directories;
      }
   }
}
