﻿using System;
using System.IO;
using System.Linq;
using HtmlAgilityPack;

namespace KFDExporter
{
   public class Post
   {
      /// <summary>
      /// Unique integer id for this post
      /// </summary>
      public string PostId { get; private set; }

      /// <summary>
      /// Integer post number, zero-based
      /// </summary>
      public string Number { get; private set; }

      /// <summary>
      /// Post subject
      /// </summary>
      public string Subject { get; private set; }

      /// <summary>
      /// KFD username of this post's author
      /// </summary>
      public string Author { get; private set; }

      /// <summary>
      /// Initial posting date. May be neither ISO 8601 nor epoch format
      /// </summary>
      public string CreationDate { get; private set; }

      /// <summary>
      /// Post edit date. Same as CreationDate if no edits. May be neither ISO 8601 nor epoch format
      /// </summary>
      public string EditDate { get; private set; }

      /// <summary>
      /// Post body
      /// </summary>
      public string Text { get; private set; }

      public static string BadDateTag = "***";

      public void Parse( HtmlNodeCollection tableDataCells )
      {
         foreach ( var cellChildNode in tableDataCells )
         {
            if ( cellChildNode.Attributes.Contains( "class" ) && cellChildNode.Attributes["class"].Value == "farRightCell" )
            {
               Subject = ParsePostSubject( cellChildNode );
               Number = ParsePostNumber( cellChildNode );
               CreationDate = ParsePostCreationDate( cellChildNode );
               EditDate = ParsePostEditDate( cellChildNode );
               Text = ParsePostText( cellChildNode );
            }
            else
            {
               Author = ParsePostAuthor( cellChildNode );
               PostId = ParsePostId( cellChildNode );
            }
         }
      }

      private static string ParsePostSubject( HtmlNode postDetailsNode )
      {
         var nodes = postDetailsNode.SelectNodes( ".//span" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "id" ) && node.Attributes["id"].Value.StartsWith( "subjectNode_" ) )
            {
               return node.InnerText;
            }
         }
         throw new MalformedHtmlFileException( "Post subject missing" );
      }

      private static string ParsePostNumber( HtmlNode postDetailsNode )
      {
         var nodes = postDetailsNode.SelectNodes( ".//a" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "name" ) && node.Attributes["name"].Value.StartsWith( "post_number_" ) )
            {
               return FixPostNumber( node.InnerText );
            }
         }

         return "0"; // the first post in a thread doesn't have a post number
      }

      private static string FixPostNumber( string postNumberFromFile )
      {
         char[] delimiters = { '.' };
         string[] postNumberParts = postNumberFromFile.Split( delimiters, StringSplitOptions.RemoveEmptyEntries );
         return postNumberParts[0];
      }

      private static string ParsePostCreationDate( HtmlNode postDetailsNode )
      {
         string date = ParseDate( postDetailsNode, "post_date" );
         return date;
      }

      private static string ParsePostEditDate( HtmlNode postDetailsNode )
      {
         string date = ParseDate( postDetailsNode, "post_edit_date" );
         return date;
      }

      private static string ParseDate( HtmlNode postDetailsNode, string nodeText )
      {
         var nodes = postDetailsNode.SelectNodes( ".//span" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "id" ) && node.Attributes["id"].Value.StartsWith( nodeText ) )
            {
               var date = node.InnerText;
               try
               {
                  return Utility.GetDate( date );
               }
               catch ( IndexOutOfRangeException exception )
               {
                  var message = String.Format( "Unparseable date \"{0}\" in {1}\r\n", node.InnerText, FileProcessor.CurrentFilename );
                  File.AppendAllText( Program.CommandlineOptions.ErrorFilename, message );
                  return BadDateTag + node.InnerText;
               }
            }
         }

         throw new MalformedHtmlFileException( "Missing post date" );
      }

 
      private static string ParsePostText( HtmlNode postDetailsNode )
      {
         var nodes = postDetailsNode.SelectNodes( ".//span" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "id" ) && node.Attributes["id"].Value.StartsWith( "textNode" ) )
            {
               return Utility.CleanHtml( node.InnerHtml );
            }
         }

         throw new MalformedHtmlFileException( "Missing post text" );
      }

      private static string ParsePostAuthor( HtmlNode postInfoNode )
      {
         foreach ( var node in postInfoNode.ChildNodes )
         {
            if ( node.Attributes.Contains( "id" ) && node.Attributes["id"].Value.StartsWith( "WPC-action_accountMenu" ) )
            {
               char[] delimiters = { '?', '=', '&' };
               var details = node.Attributes["id"].Value.Split( delimiters, StringSplitOptions.RemoveEmptyEntries );
               return details[2];
            }
         }
         throw new MalformedHtmlFileException( "Post author missing" );
      }

      private static string ParsePostId( HtmlNode postInfoNode )
      {
         var nodes = postInfoNode.SelectNodes( ".//a[@name]" );
         if ( nodes.Any() )
         {
            var postId = nodes.First().Attributes["name"].Value;
            postId = postId.Replace( "post", "" );
            return postId;
         }

         throw new MalformedHtmlFileException( "Post Id missing" );
      }
   }
}
