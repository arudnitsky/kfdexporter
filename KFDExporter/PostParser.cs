﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using System.Diagnostics;

namespace KFDExporter
{
   public class PostParser
   {
      public List<Post> Parse( HtmlDocument doc )
      {
         var posts = new List<Post>();

         var table = FindPostsTable( doc );
         var tableRows = table.SelectNodes( ".//tr" );
         if ( tableRows == null || tableRows.Count == 0 )
         {
            throw new PostParsingException( "No posts found" );
         }

         foreach ( var tableRow in tableRows )
         {
            var post = new Post();

            var tableDataCells = tableRow.SelectNodes( ".//td" );
            post.Parse( tableDataCells );

            posts.Add( post );
         }

         return posts;
      }
 
      private static HtmlNode FindPostsTable( HtmlDocument doc )
      {
         var nodes = doc.DocumentNode.SelectNodes( "//div" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "id" ) && node.Attributes["id"].Value == "threadList" )
            {
               return FindTableInThreadList( node );
            }
         }

         throw new PostParsingException( "No posts table found" );
      }

      private static HtmlNode FindTableInThreadList(HtmlNode node)
      {
         var nodesToSearch = node.SelectNodes(".//div");
         foreach (var childNode in nodesToSearch)
         {
            if (childNode.Attributes.Contains("class") && childNode.Attributes["class"].Value == "tableList")
            {
               return childNode;
            }
         }

         throw new PostParsingException("No posts table found");
      }
   }
}
