﻿using System;
using System.IO;
using System.Linq;

namespace KFDExporter
{
   class TextExporter : IExporter
   {
      private readonly string _filename;
      private StreamWriter _writer;

      public TextExporter( string filename )
      {
         _filename = filename;
      }

      public void Prepare()
      {
      }

      public void Finish()
      {
      }

      public void Export(Discussion discussion)
      {
         using ( _writer = new StreamWriter( _filename, true ) )
         {
            ExportDiscussion( discussion );
         }
      }

      private void ExportDiscussion(Discussion discussion )
      {
         _writer.WriteLine( "ThreadId: {0}", discussion.ThreadId );
         _writer.WriteLine( "Title: {0}", discussion.Title );
         _writer.WriteLine( "Author: {0}", discussion.Author );
         _writer.WriteLine( "CreationDate: {0}", discussion.CreationDate );
         _writer.WriteLine( "Category: {0}", discussion.Category );
         _writer.WriteLine( "NumReplies: {0}", discussion.NumReplies );
         _writer.WriteLine();

         _writer.WriteLine( "Posts: " );
         foreach ( var post in discussion.Posts.Select( kvPair => kvPair.Value ) )
         {
            ExportPost( post );
         }
         _writer.WriteLine();
      }

      private void ExportPost( Post post )
      {
         _writer.WriteLine( "--------------------------------------" );
         _writer.WriteLine( "PostId: {0}", post.PostId );
         _writer.WriteLine( "Number: {0}", post.Number );
         _writer.WriteLine( "Subject: {0}", Utility.CleanHtml( post.Subject ) );
         _writer.WriteLine( "Author: {0}", post.Author );
         _writer.WriteLine( "CreationDate: {0}", post.CreationDate );
         _writer.WriteLine( "EditDate: {0}", post.EditDate );
         _writer.WriteLine();
         _writer.WriteLine( Utility.CleanHtml( post.Text ) );
         _writer.WriteLine();
      }

   }
}
