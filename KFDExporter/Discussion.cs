﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using HtmlAgilityPack;

namespace KFDExporter
{
   public class Discussion
   {
      /// <summary>
      /// A unique integer id
      /// </summary>
      public string ThreadId { get; private set; } 

      /// <summary>
      /// Thread title
      /// </summary>
      public string Title { get; private set; } 
      
      /// <summary>
      /// KFD username of the original author
      /// </summary>
      public string Author { get; private set; }

      /// <summary>
      /// Creation date of this thread
      /// </summary>
      public string CreationDate { get; private set; }

      /// <summary>
      /// Where the post was placed on KFD. e.g. Discussion Forum / General Discussion
      /// </summary>
      public string Category { get; private set; }

      /// <summary>
      /// Count of replies to the original post
      /// </summary>
      public string NumReplies { get; private set; }

      /// <summary>
      /// Posts sorted by zero-based post number
      /// </summary>
      public SortedDictionary<int, Post> Posts { get; private set; }

      public Discussion()
      {
         Posts = new SortedDictionary<int, Post>();
      }

      public void MergePosts( List<Post> posts )
      {
         foreach ( var post in posts )
         {
            var postNumber = int.Parse( post.Number ); 
            if ( ! Posts.ContainsKey( postNumber ) )
            {
               Posts[postNumber] = post;
            }
            else
            {
              if ( ! post.CreationDate.Contains( Post.BadDateTag ) )
              {
                 Posts[postNumber] = post;
              }
            }
         }

         if ( Posts.ContainsKey( 0) )
         {
            Author = Posts[0].Author;
            CreationDate = Posts[0].CreationDate;
         }

         NumReplies = ( Posts.Count - 1 ).ToString( CultureInfo.InvariantCulture );
      }

      public void Parse( HtmlDocument doc )
      {
         Title = ParseDiscussionTitle( doc );
         Category = ParseDiscussionCategory( doc );
         ThreadId = ParseDiscussionThreadId( doc );
      }

      private static string ParseDiscussionTitle( HtmlDocument doc )
      {
         var nodes = doc.DocumentNode.SelectNodes( "//span" );
         if ( nodes != null )
         {
            foreach ( var node in nodes )
            {
               if ( node.Attributes.Contains( "class" ) && node.Attributes["class"].Value == "discussion" )
               {
                  string discussionTitle = node.NextSibling.InnerText;
                  return Utility.CleanHtml( discussionTitle );
               }
            }
         }
         else
         {
            throw new MalformedHtmlFileException( "Missing discussion title" );
         }
         return "";
      }

      private static string ParseDiscussionCategory( HtmlDocument doc )
      {

         var nodes = doc.DocumentNode.SelectNodes( "//div" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "class" ) && node.Attributes["class"].Value == "forumName" )
            {
               var anchors = node.ChildNodes.Where( n => n.Name == "a" );
               if ( anchors.Any() )
               {
                  string category = "";
                  foreach ( var anchor in anchors )
                  {
                     category += Utility.RemoveNbsp( anchor.InnerText ) + " / ";
                  }
                  category = category.TrimEnd( ' ', '/' ); 
                  return category;
               }

               throw new MalformedHtmlFileException( "Missing category" );
            }
         }

         throw new MalformedHtmlFileException( "Missing category" );
      }

      private static string ParseDiscussionThreadId( HtmlDocument doc )
      {
         var nodes = doc.DocumentNode.SelectNodes( "//a" );
         foreach ( var node in nodes )
         {
            if ( node.Attributes.Contains( "id" ) && node.Attributes["id"].Value.StartsWith( "WPC-action_watchThread?threadId=" ) )
            {
               string id = node.Attributes["id"].Value;
               char[] delimiters = { '?', '=', '&' };
               string[] idParts = id.Split( delimiters, StringSplitOptions.RemoveEmptyEntries );

               return idParts[2];
            }
         }
         throw new MalformedHtmlFileException( "Missing Thread Id" );
      }
   }
}