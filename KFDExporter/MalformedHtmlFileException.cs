﻿using System;

namespace KFDExporter
{
   [Serializable]
   public class MalformedHtmlFileException : Exception
   {
      public MalformedHtmlFileException()
      {
      }
      public MalformedHtmlFileException( string message ) : base( message )
      {
      }
      public MalformedHtmlFileException( string message, Exception inner ) : base( message, inner )
      {
      }
      protected MalformedHtmlFileException(
       System.Runtime.Serialization.SerializationInfo info,
       System.Runtime.Serialization.StreamingContext context )
         : base( info, context )
      {
      }
   }
}
