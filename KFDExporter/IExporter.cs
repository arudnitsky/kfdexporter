﻿
namespace KFDExporter
{
   interface IExporter
   {
      void Prepare();
      void Export(Discussion discussion);
      void Finish();
   }
}
